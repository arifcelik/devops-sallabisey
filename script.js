const express = require('express');
const app = express();
const args = require('minimist')(process.argv.slice(2));
const { Client } = require('pg');

const port = args.port || 8080;
const host = '0.0.0.0'
const connections = {
    user: "postgres",
    password: "sifre25",
    host: "localhost",
    port: 5432,
    database: "kisiler"
};

app.set('view engine', 'ejs')
app.use('/assets', express.static('assets'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

(function baglanti_deneme() {
    const client = new Client(connections);

    client.connect()
        .then(() => {
            console.log('veri tabanına bağlandı')
        })
        .catch(err => {
            console.error(err);
            process.exit(1);
        })
        .finally(() => {
            client.end()
        });
})()

async function getAll() {
    const client = new Client(connections);

    await client.connect();
    const res = await client.query('SELECT * FROM kisiler');
    await client.end();
    return res.rows;
}

function deleteById(id) {
    const client = new Client(connections);

    client.connect();
    return new Promise((resolve, reject) => {
        client.query(`DELETE FROM kisiler WHERE id = ${id}`, (err, res) => {
            if (err)
                reject(err);
            else
                resolve(res);
        });
    });
}

function add(ad, soyad, veriler) {
    const client = new Client(connections);

    client.connect();
    return new Promise((resolve, reject) => {
        client.query(`INSERT INTO kisiler (ad, soyad, veriler) VALUES ('${ad}', '${soyad}', ARRAY ${veriler})`, (err, res) => {
            if (err)
                reject(err);
            else
                resolve(res);
        });
    });
}

async function getById(id) {
    const client = new Client(connections);

    await client.connect();
    const res = await client.query('SELECT * FROM kisiler WHERE id = $1', [id]);
    await client.end();
    return res.rows[0];
}

app.get('/', async (req, res) => {
    const kisiler = await getAll();
    res.render('index', { kisiler });
    console.log(kisiler);
})

app.get('/profil', async (req, res) => {
    const veri =
        req.query.id
        ? await getById(req.query.id) || { hata: true }
        : undefined;
    console.log('profil verisi :', veri);
    res.render('profile', veri)
})

app.get('/add', (req, res) => {
    res.render('add')
})

app.post('/add', (req, res) => {
    console.log("gelen veriler :", req.body);
    const veriler = '[' + req.body.veriler.split('\r\n').map(v => `'${v}'`).join(',') + ']'
    add(req.body.ad, req.body.soyad, veriler)
    res.redirect('/');
})

app.get("/rm", (req, res) => {
    deleteById(req.query.id)
        .then(() => res.redirect('/'))
        .catch(err => res.send(err));
})

app.listen(port, host, console.log(`${host}:${port} dinleniyor...`))
