CREATE TABLE kisiler (
    id serial PRIMARY KEY,
    ad varchar(50) NOT NULL,
    soyad varchar(50) NOT NULL,
    veriler text [] NOT NULL
);

INSERT INTO kisiler (ad, soyad, veriler) 
VALUES 
('john', 'travolta', ARRAY ['pulp fiction', 'diğerleri kötü', 'gerçi bu da kötü']),
('finn', 'mertens', ARRAY ['best flute player', 'iyi kalp']),
('jake', '', ARRAY ['best friend', 'best dog', 'her şekile giriyor işte'])
